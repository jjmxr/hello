package com.example.demo.service.impl;

import com.example.demo.entity.Yaojianju;
import com.example.demo.entity.YaojianjuCopy;
import com.example.demo.mapper.YaoJianJuCopyMapper;
import com.example.demo.mapper.YaoJianJuMapper;
import com.example.demo.service.HelloService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class HelloServiceImpl implements HelloService {
    @Autowired
    private YaoJianJuCopyMapper yaoJianJuCopyMapper;
    @Autowired
    private YaoJianJuMapper yaoJianJuMapper;

    @Transactional
    @Override
    public Map<String, Object> insertYaojianjuCopy(int pageNum, int pageSize) {
        insert(pageNum, pageSize);
        return null;
    }

    private void insert(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        pageNum=1;
        List<Yaojianju> sel = null;
        sel = yaoJianJuMapper.sel();
        List<YaojianjuCopy> yaojianjuCopyList = new ArrayList<>();
        Page<Yaojianju> page = (Page<Yaojianju>) sel;
        PageInfo<Yaojianju> pageInfo = page.toPageInfo();
        for (int i = 1; i <=pageInfo.getPages(); i++) {
            PageHelper.startPage(pageNum++,pageSize );
            sel=yaoJianJuMapper.sel();
            sel.stream().forEach(yaojianju -> {
                YaojianjuCopy yaojianjuCopy = new YaojianjuCopy();
                BeanUtils.copyProperties(yaojianju, yaojianjuCopy);
                yaojianjuCopyList.add(yaojianjuCopy);
            });
            yaoJianJuCopyMapper.insert(yaojianjuCopyList);
            yaojianjuCopyList.clear();
        }
    }
}
