package com.example.demo.mapper;


import com.example.demo.entity.Yaojianju;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface YaoJianJuMapper  {
    List<Yaojianju> sel();
    int selCount();
}
