package com.example.demo.mapper;


import com.example.demo.entity.Yaojianju;
import com.example.demo.entity.YaojianjuCopy;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface YaoJianJuCopyMapper {
    List<Yaojianju> sel();
    int insert(List<YaojianjuCopy> list);
    List<YaojianjuCopy> selByIds(@Param("ids") List<Integer> ids);
    List<Integer> selIds();

    int updateTime(@Param("date") Date date);
}
