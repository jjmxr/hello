package com.example.demo.test;

import lombok.Data;

@Data
public class TestService {

    private String a;
    private String b;
    private TestService2 testService2;
}
