package com.example.demo.config;

import com.example.demo.entity.Address;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.logging.Logger;
@Slf4j
public class HelloInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

      log.info("{} 经过了拦截器",((HandlerMethod)handler).getMethod().getName());
        System.out.println(((HandlerMethod)handler).getMethod().getName());
        HttpSession session = request.getSession();
        Address user = (Address)(session.getAttribute("user"));
        System.out.println(user);
        return true;
    }
}
