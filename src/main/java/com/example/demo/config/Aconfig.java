package com.example.demo.config;

import com.example.demo.test.TestService;
import com.example.demo.test.TestService2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class Aconfig {
    @Bean
    public TestService2 testService2() {
        return new TestService2();
    }

    @Bean
    public TestService testService(TestService2 testService2) {
        TestService testService = new TestService();
        testService.setTestService2(testService2);
        return new TestService();
    }
}
