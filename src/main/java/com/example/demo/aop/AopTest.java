package com.example.demo.aop;

import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Aspect
@Component
@Slf4j
public class AopTest {

    @Pointcut("execution(* com.example.demo.controller.HelloController.*(..))")
    public void test(){

    }
    @Around("test()")
    public Object around(ProceedingJoinPoint joinpoint){
        System.out.println("aop前置执行");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Object proceed=null;
        try {
            proceed = joinpoint.proceed();
            System.out.println("aop后置执行");
            log.info("日志打印");
            stopWatch.stop();
            log.info("方法：{},总耗时：{}",joinpoint.getSignature().getName(),stopWatch.getTotalTimeMillis());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }finally {
            return proceed;
        }

    }
}
