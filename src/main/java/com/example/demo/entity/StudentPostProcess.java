package com.example.demo.entity;

public class StudentPostProcess implements PostProcess<Student> {
    @Override
    public Student process(Student student) {
        student.car.color="绿色";
        return student;
    }
}
