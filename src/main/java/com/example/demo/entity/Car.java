package com.example.demo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@ApiModel
public class Car {
    @ApiModelProperty(value = "品牌",name = "输入品牌")
    Brand brand;
    @ApiModelProperty(value = "颜色",name = "输入颜色")
    String color;
}
