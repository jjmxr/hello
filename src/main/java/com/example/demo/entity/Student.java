package com.example.demo.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Student {
    int id;
    Car car;
    String name;

    public Student build(Student student,PostProcess<Student> postProcess){

        student.id=10;
        return postProcess.process(student);
    }
}
