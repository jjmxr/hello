package com.example.demo.entity;

public interface PostProcess<T> {
    T process(T t);
}
