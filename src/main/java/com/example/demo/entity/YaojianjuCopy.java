package com.example.demo.entity;


import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * (Yaojianju)实体类
 *
 * @author makejava
 * @since 2020-02-22 17:32:40
 */
@Data
//@TableName(value = "yaojianju")
@ToString
public class YaojianjuCopy implements Serializable {
    private static final long serialVersionUID = -31392945706251328L;
    private int id;
    private String appnum;
    
    private String code;
    
    private String drugname;
    
    private String goodname;
    
    private String spec;
    
    private String packagespec;
    
    private String unit;
    
    private String manufacturer;
    
    private String remark;


}