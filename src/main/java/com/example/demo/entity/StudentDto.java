package com.example.demo.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class StudentDto {
    int id;
    Car car;
    String name;
}
