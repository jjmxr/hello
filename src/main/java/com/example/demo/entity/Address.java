package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Address {
    String country;
    String province;
    BigDecimal price;
public Address(String country){
    country=country;
}
}
