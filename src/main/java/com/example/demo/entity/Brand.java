package com.example.demo.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Brand {
    Address address;
    String mark;

}
