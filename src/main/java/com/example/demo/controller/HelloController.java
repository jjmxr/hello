package com.example.demo.controller;



import com.example.demo.entity.*;
import com.example.demo.mapper.UserMapper;
import com.example.demo.mapper.YaoJianJuCopyMapper;
import com.example.demo.mapper.YaoJianJuMapper;
import com.example.demo.service.HelloService;
import com.example.demo.test.TestService;
import com.example.demo.tools.ContextUtills;
import com.example.demo.tools.HttpClientUtil;
import io.swagger.annotations.*;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import org.springframework.web.multipart.MultipartFile;


import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("hello")
@Api(value = "hello接口", tags = {"hello接口测试swagger"})
public class HelloController {

    @Value("wo.ee")
    String a;

    private RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "1", method = RequestMethod.POST)
    public Map hello(@RequestBody(required = false) List<Student> student) {
        StopWatch stopWatch = new StopWatch();


        System.out.println(ContextUtills.getRequest().getRemoteHost());
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpMethod post = HttpMethod.POST;

        HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(httpHeaders,httpHeaders);

        System.out.println(a);
//        System.out.println(student.build(student, new PostProcess<Student>() {
//            @Override
//            public Student process(Student student) {
//                student.car.color="绿色";
//                return student;
//            }
//        }));
//        StudentDto studentDto = new StudentDto();
//        student.stream().forEach(t->{
//            BeanUtils.copyProperties(t, studentDto);
//        });
//        System.out.println("studnetDto  "+studentDto);
        List<Student> students = new ArrayList<>();

        students.addAll(student);
        List<Integer> list = students.stream().map(Student::getId).collect(Collectors.toList());


        System.out.println(list);
        HashMap<String, String> map = new HashMap<>();
        map.put("a", "1");
        map.put("b", "yun");
        stopWatch.start("请求接口");
        String s = HttpClientUtil.doGet("http://localhost:8080/hello/2", map);
        stopWatch.stop();
        System.out.println(s);
        System.out.println(stopWatch.prettyPrint());
        System.out.println(stopWatch.shortSummary());
        return null;
    }

    @RequestMapping(value = "2", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> hello2(@RequestParam Map<String, String> map) {
        System.out.println(map);
        System.out.println("接口请求了");
        map.put("1", "1");
        map.put("2", "2");
        return map;
    }

    @Autowired
    TestService testService;

    @RequestMapping(value = "3", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "3接口")
    public Map<String, Object> hello3(@RequestParam Map<String, Object> map) {
        System.out.println(map);
        System.out.println("接口请求了");
        map.put("1", "1");
        map.put("2", "2");
        List list = (List) map.get("list");
        System.out.println(list);

        System.out.println(testService);
        return map;
    }

    @Autowired
    private YaoJianJuMapper yaoJianJuMapper;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private YaoJianJuCopyMapper yaoJianJuCopyMapper;
    @Autowired
    private HelloService helloService;
    @RequestMapping("selUser/{pageNum}/{pageSize}")
    @ApiOperation(value = "插入药监局数据")
    public void testInser(@PathVariable int pageNum,@PathVariable int pageSize) {
        helloService.insertYaojianjuCopy(pageNum,pageSize );

    }

    @RequestMapping(value = "4", method = RequestMethod.POST)
    @ApiOperation(value = "测试swagger功能")
    public Object hello4(@ApiParam @RequestBody(required = false) Car car) {
        List list = new ArrayList();
        list.add("fjaj");
        list.add("fadjfjdfws");
        list.add(car);
        return list;
    }
    @ApiOperation(value = "上传接口")
    @RequestMapping(value = "upload",method = RequestMethod.POST)
    public Object upload(@RequestBody(required = true) MultipartFile multipartFile) throws IOException {
        InputStream inputStream = multipartFile.getInputStream();
        IOUtils.copy(inputStream,new FileOutputStream(new File("E://a.png")));
        return null;
    }
    @Autowired
    private HttpServletResponse response;
    @ApiOperation(value = "下载接口")
    @RequestMapping(value = "down",method = RequestMethod.POST)
    public Object download() throws IOException {
        ServletOutputStream outputStream = response.getOutputStream();
        File file = new File("E://a.png");
        FileInputStream inputStream = new FileInputStream(file);
        byte[] bytes=new byte[1024];
        int lenth;
        while ((lenth=inputStream.read(bytes))!=-1){
            outputStream.write(bytes,0 ,lenth );
            outputStream.flush();
        }
        return null;
    }

    @RequestMapping("selByIds")
    @ApiOperation(value = "查询药监局数据")
    public Object selByIds(){
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("list为空");
        List<YaojianjuCopy> yaojianjuCopyList = yaoJianJuCopyMapper.selByIds(null);
        System.out.println(yaojianjuCopyList.size());
        stopWatch.stop();
        List<Integer> list = yaoJianJuCopyMapper.selIds();
        stopWatch.start("list不为空");
        List<YaojianjuCopy> yaojianjuCopyList1 = yaoJianJuCopyMapper.selByIds(list);
        System.out.println(yaojianjuCopyList1.size());
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        return null;
    }
    @RequestMapping("insertTime")
    @ApiOperation(value = "数据库插入时间")
    public Object insertTime(){

        int i = yaoJianJuCopyMapper.updateTime(new Date());
        System.out.println("影响的行数："+i);
        return i;
    }
}
